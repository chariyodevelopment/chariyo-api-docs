FORMAT: 1A

# Chariyo.com Api

# Authentication
Api Authentication Resources

## Device login [POST /device-login]
Login the user with the mobile device ID

+ Request (application/json)

    + Attributes
        + device_id (string, required) - Login user with the current device id
    + Body

            {
                "device_id": "123456789"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "api_token"
            }

## Facebook login [POST /fb-login]
Login the user with the mobile device ID

+ Request (application/json)

    + Attributes
        + fb_token (string, required) - Login user with a facebook token.
    + Body

            {
                "fb_token": "412k43245io2152k5143219"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "api_token"
            }

## Register user [POST /register]
Register a new user and link him to a deviceID.

+ Request (application/json)

    + Attributes
        + first_name (string, required) - 
        + last_name (string, required) - 
        + device_id (string, required) - 
        + profile[gender] (string, required) - 
        + profile[birthday_year'] (string, required) - 
    + Body

            {
                "first_name": "Name",
                "last_name": "Surname",
                "email": "email@email.com",
                "device_id": "4nh32142oij4321",
                "profile": {
                    "birthday_year": "1987",
                    "gender": "m"
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "api_token"
            }

# Users [/users]

## Show user [GET /users{?include,picture}]
Get JSON data of the currently authenticated user

+ Request (application/json)

    + Attributes
        + picture (integer, optional) - Get the user's avatar picture
        + include: donations,supported_project (string, optional) - Get the possible includes for the user.

+ Response 200 (application/json)

    + Attributes
        + supported_project (object, optional) - Returns a Project object instance.
        + donations (collection, optional) - Returns a collection of the last 5 users' donations.
    + Body

            {
                "data": {
                    "id": 1,
                    "first_name": "Matej",
                    "last_name": "Vrabec",
                    "full_name": "Matej Vrabec",
                    "age": 30,
                    "birthday": "1987-04-05",
                    "gender": "m",
                    "donations": {},
                    "supported_project": {
                        "data": {}
                    }
                }
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "Picture must be numeric or empty!"
            }

# Campaigns [/campaigns]
Campaign resources

## Available campaigns [GET /campaigns/available]
Get all available campaigns for the currently authenticated user

## Repeating campaigns [GET /campaigns/repeating]
Get all the repeating campaigns that are currently unavailable to the user, but will soon be.

# Donations [/donations]
Donation process

## Start donating [GET /donations/donation/{campaign_id}]
Start the donation process or resume an existing but not yet finished one.

+ Parameters
    + campaign_id: (integer, required) - ID of the campaign to be watched.

## Donate! [POST /donations/donation/{donation_id}/finish]
Complete the donation process, run modules and create transactions.

+ Parameters
    + donation_id: (integer, required) - ID of the current donation. Donate!

## Video watched +1 [POST /donations/donation/{donation_id}/video-up]
Count up the amount of times the user has seen the video during the donation process for a campaign.

+ Parameters
    + donation_id: (integer, required) - ID of the current donation. Donate!

# Projects [/projects]

## Show all projects [GET /projects{?include}]


+ Parameters
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 4,
                        "title": "Ponudi svojo pomoč mamicam študentkam",
                        "about": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "why": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "days_remaining": 27,
                        "amount_raising": 700,
                        "completed": 23,
                        "balance": 164.4,
                        "organization": {
                            "data": {
                                "id": 265,
                                "title": "Slovenska karitas",
                                "email": "karitas@chariyo.dev"
                            }
                        }
                    }
                ]
            }

## Show all featured projects [GET /projects/featured{?include}]
Usually used on the front page/home.

+ Parameters
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 4,
                        "title": "Ponudi svojo pomoč mamicam študentkam",
                        "about": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "why": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "days_remaining": 27,
                        "amount_raising": 700,
                        "completed": 23,
                        "balance": 164.4,
                        "organization": {
                            "data": {
                                "id": 265,
                                "title": "Slovenska karitas",
                                "email": "karitas@chariyo.dev"
                            }
                        }
                    }
                ]
            }

## Show project [GET /projects/{project_id}{?include}]
Show project by its ID

+ Parameters
    + project_id: (integer, required) - ID of the project to be shown.
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 4,
                    "title": "Ponudi svojo pomoč mamicam študentkam",
                    "about": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "why": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "days_remaining": 27,
                    "amount_raising": 700,
                    "completed": 23,
                    "balance": 164.4,
                    "organization": {
                        "data": {
                            "id": 265,
                            "title": "Slovenska karitas",
                            "email": "karitas@chariyo.dev"
                        }
                    }
                }
            }

## Show supported project [GET /projects/supported{?include}]
Get the supported project for the currently authenticated user

+ Parameters
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 4,
                    "title": "Ponudi svojo pomoč mamicam študentkam",
                    "about": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "why": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "days_remaining": 27,
                    "amount_raising": 700,
                    "completed": 23,
                    "balance": 164.4,
                    "organization": {
                        "data": {
                            "id": 265,
                            "title": "Slovenska karitas",
                            "email": "karitas@chariyo.dev"
                        }
                    }
                }
            }