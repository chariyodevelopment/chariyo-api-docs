# Chariyo

## User

### Login

Login callback function can be found in the `RegularUser@loginCallback`.

## Campaign

Check and convert the campaign submission into a single form.
`CampaignController@create`, `CampaignController@store`

## Ads

Create a user has already seen the ad method. Disable ad in backend. Once the user has seen the vid it should not be
available to him anymore.

The frontend part of the controller can be found in the `CampaignFrontEndController`.

## Donation process

Check verification.


## Api documentation

[Api Docs](api/documentation.md)

[Tests](Tests.md)


