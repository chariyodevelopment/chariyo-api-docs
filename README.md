FORMAT: 1A

# Chariyo.com Api

# Authentication [/auth]
Api Authentication ResourcesHeaders for calling the api should always have: [Accept: application/vnd.chariyo.v1+json].
Calling other routes is only possible by sending also the next header: [Authorization: Bearer {api_token}].

Putting locale={locale} into the parameters for a route should change the default locale from SL (Slovenian) [sl, en].n

+ Parameters:
     + locale: `locale=sl`
         + Members:
             + `sl` - Slovenian

+ Headers:

         {
             "Accept": "application/vnd.chariyo.v1+json",
             "Authorization": "Bearer [TOKEN]"
         }

Replace [TOKEN] with the appropriate token given by calling properly one of the methods below in this resource.

All resources need the Authorization HEADER but this one. Additional data cannot be requested without or
authorization fail will happen.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/134e464d7cd8e555f84d)

## Register user [POST /auth/register]
Register a new user and link him to a deviceID.

+ Parameters
    + first_name: (string, required) - User's first name.
    + last_name: (string, optional) - User's last name.
    + email: (string, required) - Users unique identifier.
    + password: (string, required) - User's password.
    + password_confirmation: (string, required) - Password confirmation
    + profile[gender]: (string, required) - User's gender. [`f`, `m`]
    + profile[birthday_year]: (integer, required) - User's birthday year.

+ Request (application/json)
    + Body

            {
                "first_name": "Name",
                "last_name": "Surname",
                "email": "email@email.com",
                "device_id": "4nh32142oij4321",
                "profile": {
                    "birthday_year": "1987",
                    "gender": "m"
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "api_token"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "could_not_create_user",
                "status_code": 500
            }

## Password login [POST /auth/login]
Login the user with the mobile device ID

+ Parameters
    + device_id: (string, required) - Login user with the current device id

+ Request (application/json)
    + Body

            {
                "device_id": "123456789"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "api_token"
            }

## Facebook login [POST /auth/fb-login]
Login the user with the mobile device ID

+ Parameters
    + fb_token: (string, required) - Login user with a facebook token.

+ Request (application/json)
    + Body

            {
                "fb_token": "412k43245io2152k5143219"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "api_token"
            }

## Request SMS Verification [POST /auth/request-sms]


+ Parameters
    + telephone: (string, required) - Telephone number, can be a simple number string or with special chars.

+ Request (application/json)
    + Body

            {
                "telephone": "40 / 552 - 625"
            }

## Report SMS Verification [POST /auth/report-sms]


+ Parameters
    + sms_code: (string, required) - Report given code on the phone.

+ Request (application/json)
    + Body

            {
                "sms_code": "1234"
            }

## Send Password Reset Email [POST /auth/password-reset-email]
Send a reset link to the given user's email address.

+ Parameters
    + sms_code: (string, required) - Report given code on the phone.

+ Request (application/json)
    + Body

            {
                "sms_code": "1234"
            }

## Post password reset [POST /auth/password-reset]
Resets password once the user inputs the provided token, email address, password and it's confirmation.

+ Parameters
    + token: (string, required) - Token provided in email.
    + email: (string, required) - User's email address.
    + password: (string, required) - Password
    + password_confirmation: (string, required) - Password confirmation.

+ Request (application/json)
    + Body

            {
                "token": "4321432143214hu41oi32pg421",
                "email": "example@example.com",
                "password": "password",
                "password_confirmation": "password"
            }

## Post password change [POST /auth/password-change]


+ Parameters
    + password: (string, required) - Old password.
    + new_password: (string, required) - New password
    + new_password_confirmation: (string, required) - New password confirmation.

+ Request (application/json)
    + Body

            {
                "password": "old_password",
                "new_password": "password",
                "new_password_confirmation": "password"
            }

# Users [/user]

## Show user [GET /user{?include,picture}]
Get JSON data of the currently authenticated user

+ Parameters
    + picture: (integer, optional) - Get the user's avatar picture
    + include: (enum[string], optional) - Possible inclusions.
        + Members
            + `supported_project` - Returns a Project object instance.
            + `donations` - Returns a collection of the last 5 users' donations.

+ Request (application/x-www-form-urlencoded)
    + Body

            picture=300&include=donations

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 1,
                    "first_name": "Name",
                    "last_name": "Surname",
                    "full_name": "Name Surname",
                    "age": 34,
                    "birthday": "1982-01-05",
                    "gender": "m",
                    "donations": {},
                    "supported_project": {
                        "data": {}
                    }
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "picture": "Picture must be numeric or empty!"
            }

## PUT


+ Parameters
    + birthday_year: (integer, required) - User's year of birth.

+ Request (application/json)
    + Body

            {
                "birthday_year": "1982"
            }

+ Response 200 (application/json)
    + Body

            {
                "message": "success",
                "status_code": 200
            }

# Projects [/project]

## Show all projects [GET /project{?include,cover}]


+ Parameters
    + cover: (integer, optional) - Get the project's cover image
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Request (application/x-www-form-urlencoded)
    + Body

            include=donations

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 4,
                        "title": "Ponudi svojo pomoč mamicam študentkam",
                        "about": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "why": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "days_remaining": 27,
                        "amount_raising": 700,
                        "completed": 23,
                        "balance": 164.4,
                        "organization": {
                            "data": {
                                "id": 265,
                                "title": "Slovenska karitas",
                                "email": "karitas@chariyo.dev"
                            }
                        }
                    }
                ]
            }

## Show all featured projects [GET /project/featured{?include,cover}]
Usually used on the front page/home.

+ Parameters
    + cover: (integer, optional) - Get the project's cover image
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Request (application/x-www-form-urlencoded)
    + Body

            include=donations

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 4,
                        "title": "Ponudi svojo pomoč mamicam študentkam",
                        "about": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "why": "Mamicam študentkam bomo pripravili pakete plenic...",
                        "days_remaining": 27,
                        "amount_raising": 700,
                        "completed": 23,
                        "balance": 164.4,
                        "organization": {
                            "data": {
                                "id": 265,
                                "title": "Slovenska karitas",
                                "email": "karitas@chariyo.dev"
                            }
                        }
                    }
                ]
            }

## Show project [GET /project/{project_id}{?include,cover}]
Show project by its ID

+ Parameters
    + cover: (integer, optional) - Get the project's cover image
    + project_id: (integer, required) - ID of the project to be shown.
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Request (application/x-www-form-urlencoded)
    + Body

            include=donations

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 4,
                    "title": "Ponudi svojo pomoč mamicam študentkam",
                    "about": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "why": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "days_remaining": 27,
                    "amount_raising": 700,
                    "completed": 23,
                    "balance": 164.4,
                    "organization": {
                        "data": {
                            "id": 265,
                            "title": "Slovenska karitas",
                            "email": "karitas@chariyo.dev"
                        }
                    }
                }
            }

## Show supported project [GET /project/supported{?include,cover}]
Get the supported project for the currently authenticated user

+ Parameters
    + cover: (integer, optional) - Get the project's cover image
    + include: (enum[string], optional) - 
        + Members
            + `donations` - Get the list of the last 5 finished donations

+ Request (application/x-www-form-urlencoded)
    + Body

            include=donations

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 4,
                    "title": "Ponudi svojo pomoč mamicam študentkam",
                    "about": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "why": "Mamicam študentkam bomo pripravili pakete plenic, otroških šamponov, vlažilnih robčkov in otroške hrane...",
                    "days_remaining": 27,
                    "amount_raising": 700,
                    "completed": 23,
                    "balance": 164.4,
                    "organization": {
                        "data": {
                            "id": 265,
                            "title": "Slovenska karitas",
                            "email": "karitas@chariyo.dev"
                        }
                    }
                }
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "no_project_supported",
                "status_code": 500
            }

## POST


+ Parameters
    + project: (integer, required) - ID of the project to be supported by the user.

+ Request (application/json)
    + Body

            {
                "project": "7"
            }

+ Response 200 (application/json)
    + Body

            {
                "message": "success",
                "status_code": 200
            }

# Campaigns [/campaign]
Campaign resources

## Available campaigns [GET /campaign/available]
Get all available campaigns for the currently authenticated user

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 2,
                        "title": "Campaign title",
                        "video": {
                            "length": 12.1
                        },
                        "amount": 1.90,
                        "company": "Example Company Name",
                        "available_in": 0
                    }
                ]
            }

## Repeating campaigns [GET /campaign/repeating]
Get all the repeating campaigns that are currently unavailable to the user, but will soon be.

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 2,
                        "title": "Campaign title",
                        "video": {
                            "length": 12.1
                        },
                        "amount": 1.90,
                        "company": "Example Company Name",
                        "available_in": 0
                    }
                ]
            }

# Donations [/donation]
Donation process callsIf donation is included in another call, then the donation data will look like this:

{
    "id": 4015,
    "campaign": "Example campaign title",
    "company": "Example company name",
    "project": "Example project title",
    "user": "Donor's first_name"
}

## Start donating [GET /donation/{campaign_id}]
Start the donation process or resume an existing but not yet finished one.

+ Parameters
    + campaign_id: (integer, required) - ID of the campaign to be donated with.

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 6864,
                    "campaign": "Sanolabor - ko gre za zdravje",
                    "company": "Sanolabor",
                    "project": "Za otroški smeh v bolnišnicah",
                    "user": "Jaqueline Quitzon",
                    "modules": [
                        {
                            "name": "feedback",
                            "amount": 0.1
                        },
                        {
                            "name": "subscribe",
                            "amount": 1.00
                        },
                        {
                            "name": "share",
                            "amount": 0.5
                        }
                    ],
                    "module": {
                        "data": {
                            "name": "video",
                            "amount": 0.3,
                            "video": {
                                "data": {
                                    "amount": 0.3,
                                    "duration": 35.03,
                                    "lq": "/media/campaign/2/video/video_lq.mp4",
                                    "url": "/media/campaign/2/video/video.mp4"
                                }
                            }
                        }
                    }
                }
            }

+ Response 424 (application/json)
    + Body

            {
                "message": "no_project_supported"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "campaign_inactive"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "project_inactive"
            }

## Donate! [POST /donation/{donation_id}/donate]
Complete the donation process, run modules and create transactions.

+ Parameters
    + donation_id: (integer, required) - ID of the current donation.

+ Response 200 (application/json)
    + Body

            {
                "message": "success",
                "status_code": 200
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "campaign_inactive"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "project_inactive"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "not_verified"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "fb_share_error"
            }

## Get campaign module [GET /donation/{donation_id}/module/{?module}]
Get a certain campaign module or return the first available.

+ Parameters
    + donation_id: (integer, required) - Donation ID.
    + module: (string, optional) - Module name. Return first available if empty. Choices: `feedback`, `share`, `subscribe`

+ Request (application/json)
    + Body

            "/module/video"

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "video",
                    "amount": 0.3,
                    "video": {
                        "data": {
                            "amount": 0.3,
                            "duration": 35.03,
                            "lq": "/media/campaign/2/video/video_lq.mp4",
                            "url": "/media/campaign/2/video/video.mp4"
                        }
                    }
                }
            }

+ Request (application/json)
    + Body

            "/module/verification"

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "verification",
                    "amount": 0.3,
                    "verification": {
                        "data": {
                            "id": 2,
                            "text": "Kaj dru017ei v rokah deklica?",
                            "correct_answer": 7,
                            "answers": {
                                "data": [
                                    {
                                        "id": 5,
                                        "text": "Otrou0161ki u0161ampon"
                                    },
                                    {
                                        "id": 6,
                                        "text": "Sprej proti komarjem"
                                    },
                                    {
                                        "id": 7,
                                        "text": "Zobno u0161u010detko"
                                    },
                                    {
                                        "id": 8,
                                        "text": "Eko kokosovo olje"
                                    }
                                ]
                            }
                        }
                    }
                }
            }

+ Request (application/json)
    + Body

            "/module/feedback"

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "feedback",
                    "amount": 0.1,
                    "feedback": {
                        "data": [
                            {
                                "id": 1,
                                "text": "Sanolabor obiu0161u010dem, kadar potrebujem:",
                                "answers": {
                                    "data": [
                                        {
                                            "id": 1,
                                            "text": "Medicinske pripomou010dke"
                                        },
                                        {
                                            "id": 2,
                                            "text": "Zdravila in prehranska dopolnila"
                                        },
                                        {
                                            "id": 3,
                                            "text": "Certificirano naravno kozmetiko"
                                        },
                                        {
                                            "id": 4,
                                            "text": "u017divila iz eko pridelave"
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            }

+ Request (application/json)
    + Body

            "/module/subscribe"

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "subscribe",
                    "amount": 1.00,
                    "subscribe": {
                        "data": {
                            "title": "Sanolaborjeve e-novice",
                            "text": "Bodite obveu0161u010deni o vseh novostih, ugodnostih in promocijah v Sanolaborjevih prodajalnah in spletni trgovini.",
                            "button": "Prijavi se!"
                        }
                    }
                }
            }

+ Request (application/json)
    + Body

            "/module/share"

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "share",
                    "amount": 0.5,
                    "share": {
                        "data": []
                    }
                }
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "module_not_available"
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "already_verified"
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "module_unavailable"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "not_verified"
            }

## Video watched +1 [PUT /donation/{donation_id}/video-up]
Count up the amount of times the user has seen the video during the donation process for a campaign.

+ Parameters
    + donation_id: (integer, required) - ID of the current donation.

+ Response 200 (application/json)
    + Body

            {
                "message": "success"
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

## Get campaign video [GET /donation/{donation_id}/video]
Get the campaign video for the current donation process.

+ Parameters
    + donation_id: (integer, required) - Donation ID.

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "video",
                    "amount": 0.3,
                    "video": {
                        "data": {
                            "amount": 0.3,
                            "duration": 35.03,
                            "lq": "/media/campaign/2/video/video_lq.mp4",
                            "url": "/media/campaign/2/video/video.mp4"
                        }
                    }
                }
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

## Get campaign verification [GET /donation/{donation_id}/verification]
Get the campaign verification question and possible answers

+ Parameters
    + donation_id: (integer, required) - Donation ID.

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "verification",
                    "amount": 0.3,
                    "verification": {
                        "data": {
                            "id": 2,
                            "text": "Kaj dru017ei v rokah deklica?",
                            "correct_answer": 7,
                            "answers": {
                                "data": [
                                    {
                                        "id": 5,
                                        "text": "Otrou0161ki u0161ampon"
                                    },
                                    {
                                        "id": 6,
                                        "text": "Sprej proti komarjem"
                                    },
                                    {
                                        "id": 7,
                                        "text": "Zobno u0161u010detko"
                                    },
                                    {
                                        "id": 8,
                                        "text": "Eko kokosovo olje"
                                    }
                                ]
                            }
                        }
                    }
                }
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

## Submit module as form [POST /donation/{donation_id}/submit]


+ Parameters
    + donation_id: (integer, required) - Donation ID.

+ Request (application/json)
    + Body

            {
                "module": "feedback",
                "feedback": {
                    "1": {
                        "type": "buttons",
                        "value": 3
                    }
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "subscribe",
                    "amount": 1.00,
                    "subscribe": {
                        "data": {
                            "title": "Sanolaborjeve e-novice",
                            "text": "Bodite obveu0161u010deni o vseh novostih, ugodnostih in promocijah v Sanolaborjevih prodajalnah in spletni trgovini.",
                            "button": "Prijavi se!"
                        }
                    }
                }
            }

+ Request (application/json)
    + Body

            {
                "module": "subscribe",
                "subscribe": true
            }

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "share",
                    "amount": 0.5,
                    "share": {
                        "data": []
                    }
                }
            }

+ Request (application/json)
    + Body

            {
                "module": "share",
                "share": true
            }

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "name": "final",
                    "status_code": 200
                }
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "not_verified"
            }

## Get next available module [GET /donation/{donation_id}/next/{?module}]
Get the next available module or return the data for the last step of the donation process or first available
module if the module parameter is null.

+ Parameters
    + donation_id: (integer, required) - Donation ID.
    + module: (string, optional) - Module name. Return first available if empty. Choices: `feedback`, `share`, `subscribe`

+ Response 200 (application/json)
    + Body

            "see submit modules or get module for 200-OK response body"

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "not_verified"
            }

## Verify campaign [POST /donation/{donation_id}/verify]
Post verification data for verifying the campaign and donation. The user cannot continue the donation without.

+ Parameters
    + donation_id: (integer, required) - Donation ID.

+ Request (application/json)
    + Body

            {
                "verification": {
                    "question": 2,
                    "answer": 107
                }
            }

+ Response 200 (application/json)
    + Body

            "see submit modules or get module for 200-OK response body"

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "wrong_answer"
            }

## Get first available module [GET /donation/{donation_id}/first]
Get the campaign video or the first available module or the last step data.

+ Parameters
    + donation_id: (integer, required) - Donation ID.

+ Response 200 (application/json)
    + Body

            "see submit modules or get module for 200-OK response body"

+ Response 400 (application/json)
    + Body

            {
                "message": "already_finished"
            }